﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Reversi
{
    public class PredictWinner
    {
        public int counter = 0; 
        public int numberOfExamples = 0;
        public string potentialWinner = " ";
        string[] strArray;
        string line;
        

        public void ReadFile()
        {
            var exammpleArray = new List<String[]>(); 
            try
            {   // Open the text file using a stream reader.
                using (StreamReader file = new StreamReader("board.txt"))
                {
                    while ((line = file.ReadLine()) != null)
                    {
                        
                        strArray = line.Split(','); 
                        
                        if(strArray[0] != "")
                        {
                            exammpleArray.Add(strArray);
                        }                       
                        

                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }

        }
    }
}
